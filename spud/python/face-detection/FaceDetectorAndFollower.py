#!/usr/bin/python3.8 -u

# ----------------------------------------------------------------------------------------------------------------------
#
# FaceDetectorAndFollower.py
# Author: Mike Schoonover
# Date: 02/07/23
#
# Seeded from 'AI Face Following.py' by Mike Soniat
#
# Function:
#
# This program detects faces from a live camera stream and sends image position data to a host controller via ethernet.
# It can also send motor control packets directly to a motor control module via ethernet.
#
# It is meant to run on the Jetson Nano or similar systems. It may possibly run on the Raspberry Pi 4 with some
# modification, albeit much slower.
#
# Dependencies:
#
# In Linux terminal or Terminal in PyCharm:
#
# pip3 install opencv-contrib-python
#
# pip3 install typing
#
# ----------------------------------------------------------------------------------------------------------------------
#
# Explanation for the -u option: !/usr/bin/python -u
#
# Python is optimised for reading in and printing out lots of data. One of these optimisation is that the standard
# input and output of the Python interpreter are buffered. That means that whenever a program tries to use one of those
# streams, the interpreted will block up the usage into large chunks and then send the chunks all in one go. This is
# faster than sending each individual read/write through separately, but obviously has the disadvantage that data can
# get 'stopped up' in the middle.
#
# The -u flag turns off this behaviour.
#
#
# ----------------------------------------------------------------------------------------------------------------------
# Notes from previous authors:
#
# AI Face Following with Jetson Nano
# Using OpenCV and Harr Cascade Classifier
# Mike Soniat
# 2022
#
# ----------------------------------------------------------------------------------------------------------------------

import time
import cv2  # type: ignore
import argparse
import numpy as np

import numpy.typing as npt
from typing import Tuple, Final, Union

from spudLink.deviceIdentifierEnum import DeviceIdentifierEnum

from remoteHandler import RemoteHandler

# simple version string
g_version: str = "MKS0207231806"

WHEEL_CONTROLLER_NAME: str = "Wheel Motor Controller"
controlWheels: bool = False
wheelControllerIPAddress: str = ""
wheelControllerPort: int = 0

# cam: cv2.VideoCapture = None

MARK_EYES: Final[bool] = True

SUCCESS: Final[int] = 0
ERROR: Final[int] = -1
NO_DATA: Final[int] = -2

EXIT_PROGRAM: Final[int] = 1000

dispW = 640
dispH = 480
flip = 0

# --------------------------------------------------------------------------------------------------
# ::processFaceDetection
#


def processFaceDetection() -> Union[Tuple[int, npt.NDArray], Tuple]:

    """
        Runs a single detection scan on the camera stream to detect faces and optionally eyes. Draws rectangles around
        all detected faces and eyes.

        Returns the center x,y of any detected faces.

        Returns a status code.

        The detection models use grayscale images, so each frame is converted to grayscale before processing by the
        model.

            :return:    on success ->           SUCCESS , NDArray with face data
                        on no data available -> NO_DATA, empty Tuple
                        on error ->             ERROR, empty Tuple

            :rtype: Union[Tuple[int, npt.NDArray], Tuple]

    """

    valid, frame = cam.read()

    if not valid:
        return NO_DATA, ()

    grayscaleFrame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    faces: Union[Tuple, npt.NDArray]

    faces = face_cascade.detectMultiScale(grayscaleFrame, 1.3, 5)

    # for loop works for empty tuple (if no data) or Numpy array if data returned

    for (x, y, w, h) in faces:

        cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 0, 255), 2)

        if MARK_EYES:

            # objX = x+w/2
            # objY = y+h/2
            # errorPan = objX-dispW/2
            # errorTilt = objY-dispH/2

            # get image of face for use by eye detection model
            faceImageInGrayscale = grayscaleFrame[y:y+h, x:x+w]
            faceImageInColor = frame[y:y+h, x:x+w]

            eyes = eye_cascade.detectMultiScale(faceImageInGrayscale)

            for (xEye, yEye, wEye, hEye) in eyes:

                cv2.rectangle(faceImageInColor, (xEye, yEye), (xEye+wEye, yEye+hEye), (255, 0, 0), 2)
                # cv2.circle(roi_color, (int(xEye+wEye/2), int(yEye+hEye/2)), 16, (255,0,0), 1)

    cv2.imshow('Spud Face Detection', frame)

    cv2.moveWindow('Spud Face Detection', 500, 50)

    if len(faces) == 0:     # catch empty tuple or empty NDArray
        return NO_DATA, ()
    else:
        return SUCCESS, faces

# end of processFaceDetection
# --------------------------------------------------------------------------------------------------

# ----------------------------------------------------------------------------------------------------------------------
# ::doRunTimeTasks
#


def doRunTimeTasks(pControllerHandler: RemoteHandler) -> int:

    """
        Performs all run time tasks in a loop. Exits when a key is pressed or an error occurs.

        :return: SUCCESS on success, ERROR on error, EXIT_PROGRAM if program to be terminated
        :rtype: int

    """

    while True:

        if cv2.waitKey(1) == ord('q'):
            return EXIT_PROGRAM

        faceData: Union[Tuple[int, npt.NDArray], Tuple]

        faceData = processFaceDetection()

        lStatus: int = faceData[0]

        if lStatus == NO_DATA:
            return SUCCESS

        if lStatus == ERROR:
            return ERROR

        if len(faceData[1]) == 0:
            return SUCCESS

        faceCoords: npt.NDArray = faceData[1]

        if lStatus == SUCCESS:
            remoteHandler.sendFaceDataToHost(faceCoords[0])

        if lStatus == EXIT_PROGRAM:
            return lStatus

        lStatus = pControllerHandler.doRunTimeTasks()

        return lStatus

# end of ::doRunTimeTasks
# ----------------------------------------------------------------------------------------------------------------------

# ----------------------------------------------------------------------------------------------------------------------
# ::handleCommandLineArgumentParser
#


def handleCommandLineArgumentParser() -> Tuple[int, bool, str, int]:

    """
        Sets up the command line argument parser and extracts various arguments.

        To specify command line options in PyCharm, click the Wrench icon in the toolbar in the Debugger pane at
        the bottom of the IDE, type the options in the Configuration/Parameters textbox.

        Alternatively, right click on 'FaceDetectorAndFollower.py' and then click 'Modify Run Configuration...'

        For example, to force the program to connect with and send control packets to the Wheel Controller enter
        the following command line arguments (adjust IP address and port as appropriate):

        -cwm -wcip 42.42.42.102 -wcport 4242

        :return: SUCCESS on success, ERROR on error
        :rtype: int

    """

    parser = argparse.ArgumentParser()

    parser.add_argument('-cwm', '--control_wheel_motors', action="store_true",
                        help="Controls the wheel motors directly in response to image detection data.")

    parser.add_argument('-wcip', '--motor_controller_ip_address', type=str, help="Ethernet IP address of the motor "
                                                                                 "controller.")

    parser.add_argument('-wcport', '--motor_controller_port', type=int, help="Ethernet port of the motor controller.")

    args = parser.parse_args()

    lControlWheelMotors: bool
    lWheelMotorControllerIPAddress: str

    if args.control_wheel_motors:
        lControlWheelMotors = True
        lWheelMotorControllerIPAddress = args.motor_controller_ip_address
        lWheelMotorControllerPort = args.motor_controller_port
        if lWheelMotorControllerIPAddress is not None and lWheelMotorControllerPort is not None:
            lStatus = SUCCESS
        else:
            lStatus = ERROR
    else:
        lControlWheelMotors = False
        lWheelMotorControllerIPAddress = ""
        lWheelMotorControllerPort = 0
        lStatus = SUCCESS

    return lStatus, lControlWheelMotors, lWheelMotorControllerIPAddress, lWheelMotorControllerPort

# end of ::handleCommandLineArgumentParser
# ----------------------------------------------------------------------------------------------------------------------

# --------------------------------------------------------------------------------------------------
# ::prepareForProgramShutdown
#
# Prepares for the program to be shut down by closing ports and releasing resources.
#


def prepareForProgramShutdown():

    """
        Prepares for the program to be shut down by closing ports and releasing resources.

    """

    remoteHandler.disconnect()
    # logAlways("Host controller ethernet port closed...")

# end of ::prepareForProgramShutdown
# --------------------------------------------------------------------------------------------------

# ----------------------------------------------------------------------------------------------------------------------
# ::setupControllerHandler
#


def setupControllerHandler() -> RemoteHandler:

    """

        Creates and prepares the ControllerHandler object for use. This object handles communications with the host
        controller device.

        :return: a reference to the ControllerHandler object
        :rtype: ControllerHandler

    """

    MAIN_CONTROLLER_ID: Final[int] = DeviceIdentifierEnum.HEAD_PI.value
    WHEEL_CONTROLLER_ID: Final[int] = DeviceIdentifierEnum.MOTOR_CONTROLLER.value
    THIS_DEVICE_ID: Final[int] = DeviceIdentifierEnum.HEAD_JETSON_NANO.value
    MAIN_CONTROLLER_NAME: Final[str] = "Spud Head Pi"

    handler = RemoteHandler(THIS_DEVICE_ID, MAIN_CONTROLLER_ID, MAIN_CONTROLLER_NAME,
                            WHEEL_CONTROLLER_ID, WHEEL_CONTROLLER_NAME,
                            wheelControllerIPAddress, wheelControllerPort,
                            prepareForProgramShutdown)

    return handler

# end of ::setupControllerHandler
# ----------------------------------------------------------------------------------------------------------------------

# ----------------------------------------------------------------------------------------------------------------------
# ::main
#


if __name__ == '__main__':

    status: int

    status, controlWheels, wheelControllerIPAddress, wheelControllerPort = \
        handleCommandLineArgumentParser()

    if status != SUCCESS:
        print("Command line argument error!")
        exit(1)

    print("adding web cam")

    # this method works for USB cameras, but doesn't seem to work for Pi Camera 2

    # cam = cv2.VideoCapture(0)
    # cam.set(cv2.CAP_PROP_FRAME_WIDTH, dispW)
    # cam.set(cv2.CAP_PROP_FRAME_HEIGHT, dispH)

    # this method works for Pi Camera 2

    cam = cv2.VideoCapture("nvarguscamerasrc ! video/x-raw(memory:NVMM), width=(int)1280, height=(int)720,format=("
                           "string)NV12, framerate=(fraction)30/1 ! nvvidconv ! video/x-raw, format=(string)BGRx ! "
                           "videoconvert !  appsink")

    # models from:
    # https://github.com/opencv/opencv/tree/master/data/haarcascades

    print("importing models...")

    face_cascade = cv2.CascadeClassifier('models/haarcascade_frontalface_default.xml')
    eye_cascade = cv2.CascadeClassifier('models/haarcascade_eye.xml')

    print("setting up communications...")

    remoteHandler: RemoteHandler = setupControllerHandler()

    print("starting run time task loop...")

    status = SUCCESS

    while status != EXIT_PROGRAM:
        status = doRunTimeTasks(remoteHandler)

    remoteHandler.disconnect()

    cam.release()
    cv2.destroyAllWindows()

# end of ::main
# ----------------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------
