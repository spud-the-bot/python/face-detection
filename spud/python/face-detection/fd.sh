#!/bin/bash

# Runs Face Detection and Follower program with Robot Wheel Control disabled.

source ~/spud/python/face-detection/venv/python3.8/bin/activate

cd ~/spud/python/face-detection

# OPENBLAS_CORETYPE=ARMV8 python3 'AI Face Following.py'

OPENBLAS_CORETYPE=ARMV8 python3 'FaceDetectorAndFollower.py'


