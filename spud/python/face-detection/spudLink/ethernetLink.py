#!/usr/bin/python3.8 -u

# ----------------------------------------------------------------------------------------------------------------------
#
# ethernetLink.py
# Author: Mike Schoonover
# Date: 07/09/21
#
# Purpose:
#
# This class handles the link to a remote device via Ethernet connections.
#
# Open Source Policy:
#
# This source code is Public Domain and free to any interested party.  Any
# person, company, or organization may do with it as they please.
#
# ----------------------------------------------------------------------------------------------------------------------

from typing import Final, List, Tuple, Optional  # also available: Set, Dict, Tuple, Optional

import select
import socket
import time

from .spudLinkExceptions import SocketBroken
from .circularBuffer import CircularBuffer

# https://mypy.readthedocs.io/en/stable/cheat_sheet_py3.html

RECEIVE_BUFFER_SIZE: Final[int] = 5000

# An attempt to make a TCP connection with a non-blocking socket will result in an error on Linux and Windows:
#  WSAEWOULDBLOCK (10035) on Windows
#  EINPROGRESS (115) on Linux
#
# The errors indicate that the connection is not yet completed as TCP takes time which the non-blocking state does
# not allow for. This is usually not really an error and the socket will be ready soon.
#

WINDOWS_WSAEWOULDBLOCK: Final[int] = 10035
LINUX_EINPROGRESS: Final[int] = 115

# ----------------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------
# class EthernetLink
#
# This class handles the link to a remote device via Ethernet connections.
#


class EthernetLink:

    HOST: int = 0  # wip mks make these Final
    CLIENT: int = 1

    # --------------------------------------------------------------------------------------------------
    # EthernetLink::__init__
    #

    def __init__(self, pRemoteDescriptiveName: str, pHostOrClientMode: int, pLocalPort: int, pRemoteIPAddress: str,
                 pRemotePort: int):

        """
            EthernetLink initializer.

            The object can either be an ethernet host and wait for a connection or it can be a client and make a
            connection to a host. The object cannot be both a host and a client.

            To be a host: pass in EthernetLink.HOST for pHostOrClientMode, specify pRemoteDescriptiveName and pLocalPort
            and pass pRemoteIPAddress in as None and pRemotePort in as None

            To be a client: pass in EthernetLink.CLIENT for pHostOrClientMode, specify pRemoteDescriptiveName and
            pLocalPort and pass pRemoteIPAddress in as the address to connect to and pRemotePort in as the port to
            connect to

            :param pRemoteDescriptiveName: a human friendly name for the connected remote device
            :type pRemoteDescriptiveName: str

            :param pHostOrClientMode: specifies whether the link should be a HOST or CLIENT
                                        a HOST receives connections while a CLIENT makes connections
            :type pHostOrClientMode: int

            :param pLocalPort: the ethernet port this device should listen on
            :type pLocalPort: int

            :param pRemoteIPAddress: the ethernet address this device should attempt to connect to if it is a client
                                        should be "" if this object is to be a host
            :type pRemoteIPAddress: str

            :param pRemotePort: the ethernet port this device should attempt to connect to if it is a client
                                    should be -1 if this object is to be a host
            :type pRemotePort: int

        """

        self.hostOrClientMode: int = pHostOrClientMode

        self.remoteDescriptiveName: str = pRemoteDescriptiveName
        self.port: int = pLocalPort

        self.remoteIPAddress: str = pRemoteIPAddress
        self.remotePort: int = pRemotePort

        self.connected: bool = False

        self.receiveBuf = CircularBuffer(RECEIVE_BUFFER_SIZE)

        self.remoteSocket: Optional[socket.socket] = None
        self.remoteSocketList: List[socket.socket] = []

        # remoteAddress -> (remote Address, remote port)
        self.remoteAddress: Tuple[str, int] = ("", 0)

        if self.hostOrClientMode == EthernetLink.HOST:
            self.serverSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.serverSocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            self.serverSocket.bind(('', self.port))
            self.serverSocket.listen(1)
            self.serverSocketList: List[socket.socket] = [self.serverSocket]
            print("Listening for Ethernet connection request on port ", self.port)

    # end of EthernetLink::__init__
    # --------------------------------------------------------------------------------------------------

    # --------------------------------------------------------------------------------------------------
    # EthernetLink::doRunTimeTasks
    #

    def doRunTimeTasks(self) -> int:

        """
            Handles ongoing tasks associated with the socket stream such as reading data and storing in a buffer.

            This function should be called often during program execution.

            :return: 0 on success, -1 on error
            :rtype: int

        """

        if not self.connected:
            return 0

        self.handleReceive()

        return 0

    # end of EthernetLink::doRunTimeTasks
    # --------------------------------------------------------------------------------------------------

    # --------------------------------------------------------------------------------------------------
    # EthernetLink::getHostOrClientMode
    #

    def getHostOrClientMode(self) -> int:

        """
            Returns the value of hostOrClientMode flag.

            :return: self.HOST or self.CLIENT
            :rtype: int

        """

        return self.hostOrClientMode

    # end of EthernetLink::getHostOrClientMode
    # --------------------------------------------------------------------------------------------------

    # --------------------------------------------------------------------------------------------------
    # EthernetLink::handleReceive
    #

    def handleReceive(self):

        """
        This function reads all available bytes from the stream and stores them in a circular buffer.

        Strangely (and stupidly) enough, Python does not have a simple method to check if bytes are available in the
        socket (such as Java's bytesAvailable). Thus, attempting to read an empty blocking socket will hang until
        data is ready or until a timeout value is reached.

        This issue is handled here by calling select.select which will return a list containing socket(s) with data
        ready. For this program, the socket is left as blocking and the normally blocking call to select is made
        non-blocking by specifying a timeout of 1. This will allow for a quick check to see if at least one byte is
        ready.

        """

        inputs = [self.remoteSocket]
        outputs = [self.remoteSocket]
        errors = [self.remoteSocket]

        # inError: List[socket] = []

        try:
            readReady, writeReady, inError = select.select(inputs, outputs, errors, 0)
        except select.error:
            print("Ethernet Connection Error in select.select")
            return

        if inError:
            print("Ethernet Connection Error")
            return

        while readReady:  # {

            # PyCharm displays if exception here...okay because caught by client code

            data = self.remoteSocket.recv(1)

            if data == b'':
                raise SocketBroken("Error 135: Ethernet Socket Connection Broken!")

            self.receiveBuf.append(int.from_bytes(data, byteorder='big'))

            readReady, writeReady, in_error = select.select(inputs, outputs, errors, 0)

        # }

    # EthernetLink::handleReceive
    # --------------------------------------------------------------------------------------------------

    # --------------------------------------------------------------------------------------------------
    # EthernetLink::attemptToConnect
    #

    def attemptToConnect(self) -> int:

        """
            Attempts to connect to a remote device. If acting as a host, connects if a pending request from a remote is
            waiting. If acting as a client, attempts to connect to a remote device.

            Only one connection at a time is allowed.

            If a new connection is accepted, returns 1; otherwise returns 0. Returns -1 on error.

            :return: 0 on no new connection, 1 if new connection accepted, -1 on error
            :rtype: int

        """

        if self.hostOrClientMode == EthernetLink.HOST:
            return self.connectAsHostToRemoteIfRequestPending()

        if self.hostOrClientMode == EthernetLink.CLIENT:
            return self.connectAsClientToRemote()

        return -1

    # EthernetLink::attemptToConnect
    # --------------------------------------------------------------------------------------------------

    # --------------------------------------------------------------------------------------------------
    # EthernetLink::connectAsHostToRemoteIfRequestPending
    #

    def connectAsHostToRemoteIfRequestPending(self) -> int:

        """
            Checks for pending connection requests and accepts one if present. Only one connection at a time is allowed.
            The remote device is expected to initiate the connection.

            If a new connection is accepted, returns 1.

            Python also has epoll, poll, and kqueue implementations for platforms that support them. They are more
            efficient versions of select. But perhaps select is more universally supported?

            https://stackoverflow.com/questions/5308080/python-socket-accept-nonblocking

            :return: 0 on no new connection, 1 if new connection accepted, -1 on error
            :rtype: int

        """

        if not self.connected:

            readable, writable, error = select.select(self.serverSocketList, [], [], 0)

            if not readable:
                return 0

            for s in readable:

                if s is self.serverSocket:

                    self.remoteSocket, self.remoteAddress = self.serverSocket.accept()
                    self.remoteSocketList.append(self.remoteSocket)
                    print("\n\nConnection accepted from " + self.remoteDescriptiveName + " at " +
                          self.remoteAddress[0] + "\n")

                    self.connected = True

                    return 1

                # else:
                #
                #     data = s.recv(1024)
                #
                #     if data:
                #         s.send(data)
                #     else:
                #         s.close()
                #         self.client_socket_list.remove(s)

        return 0

    # end of EthernetLink::connectAsHostToRemoteIfRequestPending
    # --------------------------------------------------------------------------------------------------

    # --------------------------------------------------------------------------------------------------
    # EthernetLink::connectAsClientToRemote
    #

    def connectAsClientToRemote(self) -> int:

        """
            Attempts to connect as a client to the remote device. Only one connection at a time is allowed.

            If a new connection is accepted, returns 1.

            :return: 0 on no new connection, 1 if new connection accepted, -1 on error
            :rtype: int

        """

        if not self.connected:

            self.remoteSocket = socket.socket()

            self.remoteSocket.setblocking(False)

            try:

                status: int = self.remoteSocket.connect_ex((self.remoteIPAddress, self.remotePort))

                print("connecting to wheel controller, status = ", status)  # debug mks

                if status == WINDOWS_WSAEWOULDBLOCK or status == LINUX_EINPROGRESS:
                    time.sleep(.25)  # this could be a problem if a lot of connections are made!
                    self.connected = True
                    return 1

                if status != 0:
                    return -1

            except socket.error:
                return -1

            self.connected = True

            return 1

        return 0

    # end of EthernetLink::connectAsClientToRemote
    # --------------------------------------------------------------------------------------------------

    # --------------------------------------------------------------------------------------------------
    # EthernetLink::getConnected
    #

    def getConnected(self) -> bool:

        """
            Returns the 'connected' flag which is true if connected to remote and false otherwise.
           :return: 'connected' flag
           :rtype: int
        """

        return self.connected

    # end of EthernetLink::getConnected
    # --------------------------------------------------------------------------------------------------

    # --------------------------------------------------------------------------------------------------
    # EthernetLink::disconnect
    #

    def disconnect(self) -> int:

        """
            Disconnects from the remote device by performing shutdown() and close() on the socket.
            Sets self.connected to False.

            Resets the receive buffer so that any existing data will not affect future operations.

            :return: 0 if successful, -1 on error
            :rtype: int
        """

        self.receiveBuf.reset()

        self.connected = False

        assert self.remoteSocket is not None  # see note in this file 'Note regarding Assert' for details

        try:
            self.remoteSocket.shutdown(socket.SHUT_RDWR)
        except OSError:
            print("OSError on socket shutdown...will attempt to close anyway...")
            print("  this is usually OSError: [Errno 107] Transport endpoint is not connected...")
            print("***************************************************************************************")
        try:
            self.remoteSocket.close()
        except OSError:
            raise SocketBroken("Error 252: Ethernet Socket Connection Broken!")

        print("Disconnected from " + self.remoteDescriptiveName + " at " + self.remoteAddress[0])

        return 0

    # end of EthernetLink::disconnect
    # --------------------------------------------------------------------------------------------------

    # --------------------------------------------------------------------------------------------------
    # EthernetLink::getInputStream
    #

    def getInputStream(self) -> CircularBuffer:

        """
            Returns an "InputStream" for the remote device. For the Python version of this function, actually returns a
            CircularBuffer which the EthernetLink uses to buffer the input stream.
            
            :return: reference to a CircularBuffer used to buffer the input stream for the remote device
            :rtype: CircularBuffer
        """

        return self.receiveBuf

    # end of EthernetLink::getInputStream
    # --------------------------------------------------------------------------------------------------

    # --------------------------------------------------------------------------------------------------
    # EthernetLink::getOutputStream
    #

    def getOutputStream(self) -> socket.socket:

        """
            Returns a socket for the remote device. For Python, a socket is returned rather than a Stream as might be
            done for Java (yay Java!).

            :return: reference to a socket for the remote device
            :rtype: socket
        """

        assert self.remoteSocket is not None  # see note in this file 'Note regarding Assert' for details

        return self.remoteSocket

    # end of EthernetLink::getOutputStream
    # --------------------------------------------------------------------------------------------------


# end of class EthernetLink
# ----------------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------
