# AI Face Following with Jetson Nano
# Using OpenCV and Harr Cascade Classifier
# Mike Soniat
# 2022

import time
import cv2
# from adafruit_servokit import ServoKit

PRINT_SERVO_MESSAGES: bool = False

print("starting...")

# vars
dispW = 640
dispH = 480
flip = 0
panServo = 0
tiltServo = 1
pan = 90
tilt = 120


print("adding web cam")

# this method works for USB cameras, but doesn't seem to work for Pi Camera 2

# cam = cv2.VideoCapture(0)
# cam.set(cv2.CAP_PROP_FRAME_WIDTH, dispW)
# cam.set(cv2.CAP_PROP_FRAME_HEIGHT, dispH)

# this method works for Pi Camera 2

cam = cv2.VideoCapture("nvarguscamerasrc ! video/x-raw(memory:NVMM), width=(int)1280, height=(int)720,format=("
                       "string)NV12, framerate=(fraction)30/1 ! nvvidconv ! video/x-raw, format=(string)BGRx ! "
                       "videoconvert !  appsink")

# add servos
# kit = ServoKit(channels=16)
# kit.servo[panServo].angle = pan
# kit.servo[tiltServo].angle = tilt

# import models

# models from:
# https://github.com/opencv/opencv/tree/master/data/haarcascades

print("importing models...")

face_cascade = cv2.CascadeClassifier('models/haarcascade_frontalface_default.xml')
eye_cascade = cv2.CascadeClassifier('models/haarcascade_eye.xml')


print("detecting faces...")

while True:
    ret, frame = cam.read()
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    faces = face_cascade.detectMultiScale(gray, 1.3, 5)

    for (x, y, w, h) in faces:
        cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 0, 255), 2)

        if True:
            objX = x+w/2
            objY = y+h/2
            errorPan = objX-dispW/2
            errorTilt = objY-dispH/2

            if abs(errorPan) > 15:
                pan = pan - errorPan/75
            if abs(errorTilt) > 15:
                tilt = tilt - errorTilt/75

            # fix out of range errors
            if pan > 180:
                pan = 180
                if PRINT_SERVO_MESSAGES:
                    print("Pan Out of Range")
            if pan < 0:
                pan = 0
                if PRINT_SERVO_MESSAGES:
                    print("Pan Out of Range")
            if tilt > 180:
                tilt = 180
                if PRINT_SERVO_MESSAGES:
                    print("Tilt Out of Range")
            if tilt < 0:
                tilt = 0
                if PRINT_SERVO_MESSAGES:
                    print("Tilt Out of Range")

            # kit.servo[panServo].angle = pan
            # kit.servo[tiltServo].angle = tilt

            if PRINT_SERVO_MESSAGES:
                print("coordinates ", int(pan), ',', int(tilt))

            # create a region of interest
            roi_gray = gray[y:y+h, x:x+w]
            roi_color = frame[y:y+h, x:x+w]
            eyes = eye_cascade.detectMultiScale(roi_gray)
            for (xEye, yEye, wEye, hEye) in eyes:
                cv2.rectangle(roi_color, (xEye, yEye), (xEye+wEye, yEye+hEye), (255, 0, 0), 2)
                # cv2.circle(roi_color, (int(xEye+wEye/2), int(yEye+hEye/2)), 16, (255,0,0), 1)

            break 

    cv2.imshow('Spud Face Detection', frame)

    cv2.moveWindow('Spud Face Detection', 1000, 50)

    if cv2.waitKey(1) == ord('q'):
        pan = 90
        tilt = 120
        time.sleep(1)
        # kit.servo[panServo].angle = pan
        # kit.servo[tiltServo].angle = tilt

        break

cam.release()
cv2.destroyAllWindows()
