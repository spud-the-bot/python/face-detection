#!/usr/bin/python3.8 -u

# ----------------------------------------------------------------------------------------------------------------------
#
# remoteHandler.py
# Author: Mike Schoonover
# Date: 07/04/21
#
# Purpose:
#
# Handles a connection as a Host to a Controller program via Ethernet link. Accepts an incoming connection request and
# then monitors the connection for packets from the connected Client program.
#
# Also connects to various Hosts as a Client and handles communication with those, such as Motor Controllers, LIDAR
# Controllers, Sonar Controllers, etc.
#
# Packets are verified and then made available to client code.
#
# ----------------------------------------------------------------------------------------------------------------------

import os
import time
import sys
import traceback
import numpy as np

import numpy.typing as npt
from typing import List, Tuple, Callable, Final

from spudLink.spudLinkExceptions import SocketBroken
from spudLink.packetTypeEnum import PacketTypeEnum
from spudLink.packetStatusEnum import PacketStatusEnum
from spudLink.deviceIdentifierEnum import DeviceIdentifierEnum
from spudLink.packetTool import PacketTool
from spudLink.ethernetLink import EthernetLink

# ----------------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------
# class RemoteHandler
#
# This class handles interfacing with the Controller device.
#


class RemoteHandler:

    # final Charset UTF8_CHARSET = Charset.forName("UTF-8");

    # NOTE: the Connection Tuples of (EthernetLink, PacketTool) must be added to self.connectionList in the following
    # order so they may be accessed using these constants:

    HOST_COM: Final[int] = 0
    MOTOR_CONTROLLER_COM: Final[int] = 1

    SUCCESS: Final[int] = 0
    ERROR: Final[int] = -1
    NO_ACTION: Final[int] = -2

    # --------------------------------------------------------------------------------------------------
    # RemoteHandler::__init__
    #

    def __init__(self, pThisDeviceID: int, pMainControllerID: int, pMainControllerName: str,
                 pWheelControllerID: int, pWheelControllerName: str, pWheelControllerIPAddress: str,
                 pWheelControllerPort: int, pPrepareForProgramShutdownFunction: Callable):

        """

        RemoteHandler initializer.

        This handler can connect with multiple remote Host programs and/or remote Client programs.

        The Host is usually the Main Head Pi in which case that Host will be the controlling program.

        The Clients are usually the Motor Controller and various other controllers such as for LIDAR, Head Turning,
        Sonar, etc. These connections allow this device to control the robot instead of the Main Head Pi being the
        controller.

        :param pThisDeviceID: a numeric code to identify this device on the network
        :type pThisDeviceID: int

        :param pMainControllerID: a numeric code to identify the Main Controller remote program
        :type pMainControllerID: int

        :param pMainControllerName: a human friendly name for the Main Controller remote program
        :type pMainControllerName: str

        :param pWheelControllerID: a numeric code to identify the Wheel Controller remote program
        :type pWheelControllerID: int

        :param pWheelControllerName: a human friendly name for the Wheel Controller remote program
        :type pWheelControllerName: str

        :param pWheelControllerIPAddress: the IP address of the Wheel Controller remote program
        :type pWheelControllerIPAddress: str

        :param pWheelControllerPort: the Port of the Wheel Controller remote program
        :type pWheelControllerPort: int

        """

        self.HELLO_RESPONSE_MESSAGE = "Hello from Spud Head Nano!"

        self.THIS_DEVICE_NAME = "Spud Head Nano"

        self.thisDeviceID: int = pThisDeviceID

        self.mainControllerIdentifier: int = pMainControllerID

        self.WheelControllerID: int = pWheelControllerID

        self.prepareForProgramShutdownFunction: Callable = pPrepareForProgramShutdownFunction

        # NOTE: every program should have a different port because any two might be running on the same computer

        self.port: Final[int] = 4245

        self.pktRcvCount = 0

        self.waitingForACKPkt: bool = False

        self.detectionSendTimerEnd: float = 0

        self.DETECTION_SEND_TIMER_PERIOD: Final[float] = 0.3

        self.faceDataSendTimerEnd: float = 0

        self.FACE_DATA_SEND_TIMER_PERIOD: Final[float] = 0.3

        self.MAX_SHORT_INT: Final[int] = 32767

        self.haltMovement: bool = False

        self.controlWheels: bool = False

        connectionList: List[Tuple[EthernetLink, PacketTool]]

        mainControllerEthernetLink: EthernetLink = EthernetLink(pMainControllerName, EthernetLink.HOST, self.port, "",
                                                                -1)

        mainControllerPacketTool = PacketTool(self.thisDeviceID)

        mainControllerConnection = (mainControllerEthernetLink, mainControllerPacketTool)

        self.connectionList = [mainControllerConnection]  # this will be at index HOST_COM

        if pWheelControllerIPAddress != "":

            motorControllerEthernetLink: EthernetLink = EthernetLink(pWheelControllerName, EthernetLink.CLIENT,
                                                                     self.port, pWheelControllerIPAddress,
                                                                     pWheelControllerPort)
            motorControllerPacketTool = PacketTool(self.thisDeviceID)
            motorControllerConnection = (motorControllerEthernetLink, motorControllerPacketTool)
            self.connectionList.append(motorControllerConnection)  # this will be at index MOTOR_CONTROLLER_COM
            self.controlWheels = True

    # end of RemoteHandler::__init__
    # --------------------------------------------------------------------------------------------------

    # --------------------------------------------------------------------------------------------------
    # RemoteHandler::setWaitingForACKPkt
    #

        """
        
        Sets the waitingForACKPkt flag to the specified state.
        
        :param pState: the state to which waitingForACKPkt is to be set
        :type pState: bool
        
        """

    def setWaitingForACKPkt(self, pState: bool):

        self.waitingForACKPkt = pState

    # end of RemoteHandler::setWaitingForACKPkt
    # --------------------------------------------------------------------------------------------------

    # --------------------------------------------------------------------------------------------------
    # RemoteHandler::handleEthernetConnection
    #

    def handleEthernetConnection(self, pConnection: Tuple[EthernetLink, PacketTool]) -> int:

        """

        Handles communications and actions with remote programs with this device acting as a Host or a Client
        per the type of each connection.

        This function should be called often during runtime to allow for continuous processing.

        For Host connections, if the remote program is not currently connected then this function will check for
        connection requests and accept the first one to arrive.

        For Client connections, if the remote program is not currently connected then an attempt will be made to
        connect.

        If a connection is already active, this function will monitor the incoming data stream for packets and
        process them.

        An EthernetLink object only handles one remote host or client device at a time. To handle more hosts or
        clients, a new EthernetLink object should be created for each.

        :param pConnection: the EthernetLink and PacketTool instances for the remote to be processed
        :type pConnection: Tuple[EthernetLink, PacketTool]

        :return: 0 if no operation performed, 1 if an operation performed, -1 on error
        :rtype: int

        """

        ethernetLink: EthernetLink
        packetTool: PacketTool

        ethernetLink, packetTool = pConnection

        try:

            if not ethernetLink.getConnected():
                status: int = ethernetLink.attemptToConnect()
                if status == 1:
                    packetTool.setStreams(ethernetLink.getInputStream(), ethernetLink.getOutputStream())
                    return 1
                else:
                    return 0
            else:
                return self.handleCommunications(pConnection)

        except ConnectionResetError:

            self.logExceptionInformation("Connection Reset Error - remote program probably terminated improperly.")
            self.disconnect()
            return 0

        except ConnectionAbortedError:

            self.logExceptionInformation("Connection Aborted Error - remote program probably terminated improperly.")
            self.disconnect()
            return 0

        except SocketBroken:
            self.logExceptionInformation("Host Socket Broken - Remote probably closed connection.")
            self.disconnect()
            return 0

    # end of RemoteHandler::handleEthernetConnection
    # --------------------------------------------------------------------------------------------------

    # --------------------------------------------------------------------------------------------------
    # RemoteHandler::handleEthernetConnections
    #

    def handleEthernetConnections(self) -> int:

        """

        Handles all ethernet connections, acting as Host or Client depending on the type of each connection.

        See handleEthernetConnection for more details.

        :return: -1 on error or >=0 for the number of operations performed if no error
        :rtype: int

        """

        status: int

        operationsPerformedCount: int = 0

        for (remote) in self.connectionList:

            status = self.handleEthernetConnection(remote)

            if status == 1:
                operationsPerformedCount += 1

            if status < 0:
                return status

        return RemoteHandler.SUCCESS

    # end of RemoteHandler::handleEthernetConnections
    # --------------------------------------------------------------------------------------------------

    # --------------------------------------------------------------------------------------------------
    # RemoteHandler::doRunTimeTasks
    #

    def doRunTimeTasks(self) -> int:

        """

        Handles communications and actions with remote devices. This function should be called often during
        runtime to allow for continuous processing.

        :return: 0 if no operation performed, 1 if an operation performed, -1 on error
        :rtype: int

        """

        remoteStatus: int = self.handleEthernetConnections()

        self.handleWheelMovement()

        return remoteStatus

    # end of RemoteHandler::doRunTimeTasks
    # --------------------------------------------------------------------------------------------------

    # --------------------------------------------------------------------------------------------------
    # RemoteHandler::handleWheelMovement
    #

    def handleWheelMovement(self) -> int:

        """

        Handles wheel movement by sending wheel speed commands to the Wheel Controller

        :return: 0 if no operation performed, 1 if an operation performed, -1 on error
        :rtype: int

        """

        #  debug mks
        #  registerBytes = bytes([0x7f, 0xff, 0xff, 0xff])
        #  return controllerHandler.sendBatteryDataPacket(registerBytes)
        #  debug mks end

        #  self.packetTool.sendBytes(DeviceIdentifierEnum.MOTOR_CONTROLLER.value, PacketTypeEnum.BATTERY_DATA,
        #                         pBatteryVoltageBytes)

        if not self.controlWheels:
            return 0

        if self.haltMovement:
            return 0

        print("handling wheel movement ")  # debug mks

        ethernetLink: EthernetLink
        packetTool: PacketTool

        ethernetLink, packetTool = self.connectionList[RemoteHandler.MOTOR_CONTROLLER_COM]

        if not ethernetLink.getConnected():
            return 0

        self.haltMovement = True

        numCentimeters: int = 3
        timeDurationMS: int = 2000
        leftWheelSpeed: int = 5
        rightWheelSpeed: int = 5

        command: Tuple[int, int, int, int, int, int, int, int] = (numCentimeters, numCentimeters, timeDurationMS,
                                                                  timeDurationMS, leftWheelSpeed, leftWheelSpeed,
                                                                  rightWheelSpeed, rightWheelSpeed)

        commandInts: List[Tuple] = [command]

        packetTool.sendSignedShortIntsFromListOfTuples(
            DeviceIdentifierEnum.MOTOR_CONTROLLER.value,
            PacketTypeEnum.MOVE_BY_DISTANCE_AND_TIME, commandInts)

        return 1

    # end of RemoteHandler::handleWheelMovement
    # --------------------------------------------------------------------------------------------------

    # ----------------------------------------------------------------------------------------------------------------------
    # ::sendFaceDataToHost
    #

    def sendFaceDataToHost(self, face: npt.NDArray) -> int:

        """
            Sends x,y,width,height data for the bounding box for a single head to the host.

            Will only send data if FACE_DATA_SEND_TIMER_PERIOD number of seconds have passed since the last
            transmission.

            Returns a status code.

            :return: SUCCESS on success, ERROR on error, NO_ACTION if no action performed

        """

        ethernetLink, packetTool = self.connectionList[self.HOST_COM]

        if not ethernetLink.getConnected():
            return self.NO_ACTION

        remoteDeviceID: int = packetTool.getSourceDeviceID()

        if not self.connectionList:
            return self.NO_ACTION

        nowTime: float = time.perf_counter()

        if nowTime < self.faceDataSendTimerEnd:
            return self.NO_ACTION

        self.faceDataSendTimerEnd = nowTime + self.FACE_DATA_SEND_TIMER_PERIOD

        x: int = face[0]
        y: int = face[1]
        width: int = face[2]
        height: int = face[3]

        print(remoteDeviceID)
        print(x, " : ", y, " : ", width, " : ", height)

        packetTool.sendSignedShortIntsFromListOfTuples(
            remoteDeviceID,
            PacketTypeEnum.FACE_DATA, [(x,y,width,height)])

        return self.SUCCESS

    # end of sendFaceDataToHost
    # --------------------------------------------------------------------------------------------------

    # --------------------------------------------------------------------------------------------------
    # RemoteHandler::handleCommunications
    #

    def handleCommunications(self, pConnection: Tuple[EthernetLink, PacketTool]) -> int:

        """

        Handles communications with for a connection with a remote program. This function should be called often during
        runtime to allow for continuous processing.

        This function will monitor the incoming data stream for packets and process them as they are received.

        :param pConnection: the EthernetLink and PacketTool instances for the remote to be processed
        :type pConnection: Tuple[EthernetLink, PacketTool]

        :return: 0 on no packet handled, 1 on packet handled, -1 on error
                    note that broken or disconnected sockets do NOT return an error as they are handled as a
                    normal part of the process
        :rtype: int

        :raises: SocketBroken:  if socket is broken - probably due to Host closing connection

        """

        ethernetLink: EthernetLink
        packetTool: PacketTool

        ethernetLink, packetTool = pConnection

        ethernetLink.doRunTimeTasks()

        packetReady: bool

        packetReady = packetTool.checkForPacketReady()

        if packetReady:
            return self.handlePacket(packetTool)

        self.pktRcvCount += 1

        return 0

    # end of RemoteHandler::handleCommunications
    # --------------------------------------------------------------------------------------------------

    # --------------------------------------------------------------------------------------------------
    # RemoteHandler::handlePacket
    #

    def handlePacket(self, pPacketTool: PacketTool) -> int:

        """

        Handles a packet received from a remote device.

        :param pPacketTool: the PacketTool object containing the packet to be processed
        :type pPacketTool: PacketTool

        :return: 0 on no packet handled, 1 on packet handled, -1 on error
        :rtype: int

        """

        pktType: PacketTypeEnum = pPacketTool.getPktType()

        remoteDeviceID: int = pPacketTool.getSourceDeviceID()

        if pktType is PacketTypeEnum.GET_DEVICE_INFO:

            return self.handleGetDeviceInfoPacket(remoteDeviceID, pPacketTool)

        elif pktType == PacketTypeEnum.LOG_MESSAGE:

            # todo mks
            print("Packet received of type: " + pktType.name + "\n")
            return 1

        else:

            return 0

    # end of RemoteHandler::handlePacket
    # --------------------------------------------------------------------------------------------------

    # --------------------------------------------------------------------------------------------------
    # RemoteHandler::handleGetDeviceInfoPacket
    #

    def handleGetDeviceInfoPacket(self, pRequestingDeviceIdentifier: int, pPacketTool: PacketTool) -> int:

        """

        Handles a GET_DEVICE_INFO packet by transmitting a greeting via a LOG_MESSAGE packet back to the remote
        device.

        :param pRequestingDeviceIdentifier: numeric code to identify the device requesting the action
        :type pRequestingDeviceIdentifier: int

        :param pPacketTool: the PacketTool object associated with the device requesting the action
        :type pPacketTool: int

        :return: 0 on no packet handled, 1 on packet handled, -1 on error
        :rtype: int

        """

        print("Packet received of type: GET_DEVICE_INFO")

        pPacketTool.sendString(pRequestingDeviceIdentifier, PacketTypeEnum.LOG_MESSAGE, self.HELLO_RESPONSE_MESSAGE)

        return 1

    # end of RemoteHandler::handleGetDeviceInfoPacket
    # --------------------------------------------------------------------------------------------------

    # --------------------------------------------------------------------------------------------------
    # RemoteHandler::handleShutDownOperatingSystem
    #

    def handleShutDownOperatingSystem(self, pRequestingDeviceIdentifier: int, pPacketTool: PacketTool) -> int:

        """

        Handles a SHUT_DOWN_OPERATING_SYSTEM packet by:
            closing all sockets, ports, etc.
            transmitting a response via a LOG_MESSAGE packet back to the remote host
            invoking Operating System shutdown by using a system call

        :param pRequestingDeviceIdentifier: numeric code to identify the device requesting the action
        :type pRequestingDeviceIdentifier: int

        :param pPacketTool: the PacketTool object associated with the device requesting the action
        :type pPacketTool: int

        :return: 0 on no packet handled, 1 on packet handled, -1 on error
        :rtype: int

        """

        print("Packet received of type: SHUT_DOWN_OPERATING_SYSTEM")

        index: int = 0

        pktStatus, index, value = pPacketTool.parseUnsignedByteFromPacket(index)
        if pktStatus != PacketStatusEnum.PACKET_VALID:
            return -1

        # if the data byte is 0 then perform reboot, if 1 then shutdown

        message: str

        if value == 0:
            message = "Rebooting"
        elif value == 1:
            message = "Shutting Down"
        else:
            message = "Shutting Down"

        pPacketTool.sendString(pRequestingDeviceIdentifier, PacketTypeEnum.LOG_MESSAGE, self.THIS_DEVICE_NAME +
                               " says " + message + " the Operating System!")

        self.prepareForProgramShutdownFunction()

        # if the data byte is 0 then perform reboot, if 1 then shutdown

        if value == 0:
            os.system("shutdown -r now")
        elif value == 1:
            os.system("shutdown now")
        else:
            os.system("shutdown now")

        sys.exit()

    # end of RemoteHandler::handleShutDownOperatingSystem
    # --------------------------------------------------------------------------------------------------

    # --------------------------------------------------------------------------------------------------
    # RemoteHandler::disconnect
    #

    def disconnect(self) -> int:

        """

        Disconnects from all pEthernetLink remotes and resets the pPacketTool for each which discards any partially
        read packets.

        :return: 0 if successful for all remotes, -1 on error for any remotes
        :rtype: int

        """

        disconnectStatus: int
        disconnectStatusFlag: int = 0

        ethernetLink: EthernetLink
        packetTool: PacketTool

        for (ethernetLink, packetTool) in self.connectionList:
            packetTool.reset()
            disconnectStatus = ethernetLink.disconnect()
            if disconnectStatus == -1:
                disconnectStatusFlag = -1

        return disconnectStatusFlag

    # end of RemoteHandler::disconnect
    # --------------------------------------------------------------------------------------------------

    # --------------------------------------------------------------------------------------------------
    # RemoteHandler::logExceptionInformation
    #

    @staticmethod
    def logExceptionInformation(pMessage: str):

        """

        Displays a message, the current Exception name and info, and the traceback info.

        A row of asterisks is printed before and after the info to provide separation.

        :param pMessage: the message to be displayed
        :type pMessage: str

        """

        print("***************************************************************************************")

        print(pMessage)

        print("")

        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback.print_exception(exc_type, exc_value, exc_traceback)

        print("***************************************************************************************")

    # end of RemoteHandler::logExceptionInformation
    # --------------------------------------------------------------------------------------------------

# end of class RemoteHandler
# ----------------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------
