#!/bin/bash

# Runs Face Detection and Follower program with Robot Wheel Control enabled.

source ~/spud/python/face-detection/venv/python3.8/bin/activate

cd ~/spud/python/face-detection

# OPENBLAS_CORETYPE=ARMV8 python3 'AI Face Following.py'

# this will use the Python3 version of the virtual environment

OPENBLAS_CORETYPE=ARMV8 python3 'FaceDetectorAndFollower.py' -cwm -wcip 42.42.42.102 -wcport 4244


