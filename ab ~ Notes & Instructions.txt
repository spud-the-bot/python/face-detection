

To run on Spud, use shortcut script in ~/spud folder:

	cd ~/spud
	source face-detection.sh

To perform development work:

	cd ~/spud/python/face-detection
	
	source venv.sh
	
	python3 face-detection.py
	

Note: see instructions later for creating venv.sh and face-detection.sh	

-----------------------------------------------------------------------
	
Note the user name is 'spud' and the subfolder containing Spud's files is also 'spud':

	/home/spud/spud/

-----------------------------------------------------------------------


Python & Linux Tips

-----------------------------------------------

show Python version:

python --version or python -v or python -VV
python --version or python -v or python -VV
python --version or python -v or python -VV

OR

python3 --version or python3 -v or python3 -VV

OR

python3.8 --version or python3.8 -v or python3.8 -VV

to specify a version if it is not the system default

-----------------------------------------------

Update Linux:

sudo apt-get update
sudo apt-get upgrade

-----------------------------------------------

Install Python virtual environment:

sudo apt-get update
sudo apt-get upgrade


sudo apt-get -y install python3-pip

# verify:

pip3 --version

sudo pip3 install virtualenv

-----------------------------------------------

Install the desired version of Python, at least version 3.8.0 or later.

The instructions are in:

	Install Python from Source.txt

-----------------------------------------------

Create a Python virtual environment for a project and install Python3 in it

mkdir ~/spud/python/your-project/venv 
cd ~/spud/python/your-project/venv
virtualenv -p python3 python3

to force use of a particular python version such as 3.8:

virtualenv -p python3.8 python3.8

-----------------------------------------------

Verify environment active:	
	
	source ~/spud/python/your-project/venv/python3/bin/activate<enter>
	
		command line should now display:

		(python3) spud@spud1-head-nano:~
	
	Note: the pip3 installs later in this document are done while the virtual
			environment is still activated!
	
-----------------------------------------------

Create shortcut bash script to start virtual environment for development:

in '~/spud/python/your-project' folder create file 'venv.sh':

---

#!/bin/bash

source ~/spud/python/your-project/venv/python3/bin/activate

-----

make executable:

chmod +x venv.sh<enter>

run with:

source venv.sh<enter>

-----------------------------------------------

Create shortcut bash script to start virtual environment and also the program:

in '~/spud' folder create file 'your-project.sh' (or use a convenient abbreviation such as 'yp.sh':

---

#!/bin/bash

source ~/spud/python/your-project/venv/python3/bin/activate

python3 ~/spud/python/your-project/your-project.py

-----

make executable:

chmod +x venv.sh<enter>

run with:

source your-project.sh<enter>

-----------------------------------------------

Before running your-project, you will have to install its dependencies.

Run it to see what it is missing (shortcut named 'fd.sh' in this case):

(python3) spud@spud1-head-nano:~/spud$ source fd.sh
Traceback (most recent call last):
  File "AI Face Following.py", line 7, in <module>
    import cv2
ModuleNotFoundError: No module named 'cv2'
(python3) spud@spud1-head-nano:~/spud/python/face-detection$

So, 'cv2' is a missing dependency. Sometimes you will use apt-get to install
and other times you will use pip3.

To install 'cv2' for Python (be sure the virtual environment is active):


"Again, in the vast majority of situations you will want to install opencv-contrib-python on your system...The opencv-contrib-python repository contains both the main modules along with the contrib modules — this is the library I recommend you install as it includes all OpenCV functionality."


pip3 install opencv-contrib-python<enter>

(this takes a while!)

Note: the following were installed before OpenCV, but these can probably be skipped.

sudo apt-get install libgtk2.0-dev
sudo apt-get install pkg-config

-----------------------------------------------
Testing the camera

running:

ls /dev/video0<enter>

should show if camera is working:

/dev/video0

gst-launch-1.0 nvarguscamerasrc ! 'video/x-raw(memory:NVMM),width=3820, height=2464, framerate=21/1, format=NV12' ! nvvidconv flip-method=0 ! 'video/x-raw,width=960, height=616' ! nvvidconv ! nvegltransform ! nveglglessink -e<enter>

-----------------------------------------------------------------------